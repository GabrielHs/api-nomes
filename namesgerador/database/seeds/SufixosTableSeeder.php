<?php

use Illuminate\Database\Seeder;

class SufixosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sufixos')->insert([
            'types' => 'sufix',
            'description' => 'Mart',
            'created_at' => date("Y-m-d H:i:s")
        ]);
    }
}
