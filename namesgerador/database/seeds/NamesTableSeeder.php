<?php

use Illuminate\Database\Seeder;

class NamesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('names')->insert([
            'types' => 'prefix',
            'description' => 'teste',
            'created_at' => date("Y-m-d H:i:s")
        ]);
    }
}
