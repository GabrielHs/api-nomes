<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

//Route::prefix('api')->group(function () {
//    Route::get('/nomes', 'NomesController@index');
//});

//Route::apiResource('nomes.index', 'api\NomesController');

//Route::get('/nomes', 'UserController@index');

Route::get('/nomes', 'api\NomesController@index');

Route::post('/prefixo', 'api\NomesController@createPrefix');

Route::post('/sufixo', 'api\NomesController@createSufix');

Route::delete('/prefixo/delete/{id}', 'api\NomesController@deletePrefix');

Route::delete('/sufixo/delete/{id}', 'api\NomesController@deleteSufix');

Route::get('/dominios', 'api\NomesController@domains');

Route::get('/dominio/{name}', 'api\NomesController@domainsDetails');