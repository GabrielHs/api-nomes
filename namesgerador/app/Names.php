<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Names extends Model
{
    protected $fillable = [
        'types',
        'description'
    ];


    public  $timestamps = true;


    protected $table = 'names';
}
