<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sufixos extends Model
{
    protected $fillable = [
        'types',
        'description'
    ];


    public  $timestamps = true;


    protected $table = 'sufixos';
}
