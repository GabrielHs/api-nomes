<?php

namespace App\Http\Controllers\api;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Names;
use App\Sufixos;
use App\Dominios;


class NomesController extends Controller
{
    public function index()
    {
        $uprefixo = DB::table('names')->get();

        $sufixo = DB::table('sufixos')->get();





        //dd($collection);
        //        $collection = collect([
        //            ['id' => 1, 'types' => 'prefix', 'description' => 'Air'],
        //            ['id' => 2, 'types' => 'prefix', 'description' => 'Jet'],
        //            ['id' => 3, 'types' => 'prefix', 'description' => 'Flitght'],
        //            ['id' => 4, 'types' => 'sufix', 'description' => 'Hub'],
        //            ['id' => 5, 'types' => 'sufix', 'description' => 'Station'],
        //            ['id' => 6, 'types' => 'sufix', 'description' => 'Mart'],
        //        ]);

        //Cria dois grupos uma para prefisso e sufixos
        // $groupItems = $collection->groupBy('types');

        // //Captura todos os grupos do array prefix
        // $groupPrefix = $groupItems['prefix'];

        // //Captura todos os grupos do array suffix
        // $groupSufix = $groupItems['sufix'];


        return ["prefixos" => $uprefixo, "sufixos" => $sufixo,];

        //        $prefixos = collect([
        //            ['id' => 1, 'types' => 'prefix', 'description' => 'Air'],
        //            ['id' => 2, 'types' => 'prefix', 'description' => 'Jet'],
        //            ['id' => 3, 'types' => 'prefix', 'description' => 'Flitght'],
        //
        //        ]);
        //
        //        $sufixos = collect([
        //            ['id' => 1, 'types' => 'sufix', 'description' => 'Hub'],
        //            ['id' => 2, 'types' => 'sufix', 'description' => 'Station'],
        //            ['id' => 3, 'types' => 'sufix', 'description' => 'Mart'],
        //        ]);
        //
        //
        //        return ["prefixos" => $prefixos, "sufixos" => $sufixos,];
    }


    public function createPrefix(Request $request)
    {
        try {


            $description = $request->description;

            $types = $request->types;

            $name = new Names;

            $name->types = $types;

            $name->description = $description;


            $name->save();

            return ["OK" => "O" . $description . " foi cadastrado com sucesso"];
        } catch (\Exception $e) {

            $excecao = $e->getMessage();

            return ["erro" => $excecao];
        }
    }


    public function createSufix(Request $request)
    {
        try {


            $description = $request->description;

            $types = $request->types;

            $name = new Sufixos;

            $name->types = $types;

            $name->description = $description;


            $name->save();

            return ["OK" => "O " . $description . " foi cadastrado com sucesso"];
        } catch (\Exception $e) {

            $excecao = $e->getMessage();

            return ["erro" => $excecao];
        }
    }


    public function deletePrefix($id)
    {
        try {
            if ($id != null) {

                $pref  = Names::find($id);

                $nome = $pref->description;

                $pref->delete();

                return ["OK" => "O " . $nome . " foi excluido"];
            } else {

                return ["Info" =>  "id passado esta nulo"];
            }
        } catch (\Exception $e) {
            $excecao = $e->getMessage();

            return ["Erro" => $excecao];
        }
    }

    public function deleteSufix($id)
    {
        try {
            if ($id != null) {

                $pref  = Sufixos::find($id);

                $nome = $pref->description;

                $pref->delete();

                return ["OK" => "O " . $nome . " foi excluido"];
            } else {

                return ["Info" =>  "id passado esta nulo"];
            }
        } catch (\Exception $e) {
            $excecao = $e->getMessage();

            return ["Erro" => $excecao];
        }
    }



    public function domains()
    {


        try {

            $prefixos = Names::all();

            $sufixos = Sufixos::all();

            $dados = [];

            foreach ($prefixos as $prefix) {
                foreach ($sufixos as $suffix) {
                    $name = $prefix->description . $suffix->description;

                    $url =  mb_strtolower($name);

                    $html = file_get_contents('https://www.whois.com/whois/' . $url . '.com.br');

                    preg_match('/domain:\s[^>]+?>([^<]+?)/simx', $html, $matches);


                    if ($matches) {
                        $dados[] =
                            [
                                'status' => "Dominio indisponivel",
                                'name' => $url,
                                'checkout' => "https://checkout.hostgator.com.br/?a=add&sld=" . $url. "&tld=.com.br"
                            ];
                    } else {
    
                        $dados[] =
                            [
                                'status' => "Dominio disponivel",
                                'name' => $url,
                                'checkout' => "https://checkout.hostgator.com.br/?a=add&sld=" . $url. "&tld=.com.br"
                            ];
                    }
                }
            }

            return ["OK" => collect($dados)->all()];


        } catch (\Exception $e) {
            $excecao = $e->getMessage();

            return ["Erro" => $excecao];
        }
    }


    public function domainsDetails($name)
    {

        if ($name == null) {
            return ["Erro" => "não foi passado o dominio"]; 
        }

        $extensions = [".com.br", ".com", ".net"];
        $dados = [];

        foreach ($extensions as $ext) 
        {
            
            $url =  mb_strtolower($name);

            $html = file_get_contents('https://www.whois.com/whois/' . $url . '' .$ext.'');

            preg_match('/domain:\s[^>]+?>([^<]+?)/simx', $html, $matches);


            if ($matches) {
                $dados[] =
                    [
                        'status' => "Dominio indisponivel",
                        'extension' => $ext,
                        'checkout' => "https://checkout.hostgator.com.br/?a=add&sld=" . $url. "&tld=". $ext .""
                    ];
            } else {

                $dados[] =
                    [
                        'status' => "Dominio disponivel",
                        'extension' => $ext,
                        'checkout' => "https://checkout.hostgator.com.br/?a=add&sld=" . $url. "&tld=". $ext .""
                    ];
            }
        }

        return ["Ok" => $dados];
    }



}
